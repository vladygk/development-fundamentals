﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Dotnetcore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Tasks
    {
        private static Dictionary<int, int> memo = new Dictionary<int, int>();
        private static List<int> seq = new List<int>() { 1, 1 };



        public static int[] OrderByBits(int[] input) => input.OrderByDescending(x => CalculateSumOfBits(x)).ToArray();

        public static int CalculateSumOfBits(int n)
        {
            int[] numberInBinary = new int[1000];
            for (int i = 0; n > 0; i++)
            {
                numberInBinary[i] = n % 2;
                n = n / 2;
            }

            return numberInBinary.Sum();
        }

        public static int GenerateFibonacci(int n, List<int> seq, Dictionary<int, int> memo)
        {
            if (n == 1 || n == 2)
            {
                return 1;
            }

            if (memo.ContainsKey(n))
            {
                return memo[n];
            }

            int value = GenerateFibonacci(n - 1, seq, memo) + GenerateFibonacci(n - 2, seq, memo);
            seq.Add(value);
            if (!memo.ContainsKey(n))
            {
                memo.Add(n, value);
            }

            return value;
        }

        public static bool CheckIsBorderFibonacci(List<List<int>> matrix)
        {
            List<int> border = GetBorder(matrix);
            GenerateFibonacci(30, seq, memo);
            // Console.WriteLine(String.Join(" ",border));
            if (!seq.Contains(border[0]))
            {
                return false;
            }

            int startIndex = seq.IndexOf(border[0]);
            for (int i = 0; i < border.Count; i++)
            {
                if (seq[startIndex++] != border[i])
                {
                    return false;
                }
            }

            return true;
        }

        static bool CheckIsNeighboursFibonacci(int[][] matrix)
        {
            GenerateFibonacci(10, seq, memo);
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    List<int> neighbours = GetAllNeighbours(matrix, i, j);
                    


                    if (!seq.Contains(neighbours[0]))
                    {
                        continue;
                    }

                    int startIndex = seq.IndexOf(neighbours[0]);

                    for (int z = 0; z < neighbours.Count; z++)
                    {
                        if (seq[startIndex++] != neighbours[z])
                        {
                            break;
                        }

                        if (z == neighbours.Count - 1)
                        {
                            return true;
                        }
                    }

                }
            }

            return false;

        }

        public static List<List<List<int>>> GenerateAllSquares(int[][] matrix)
        {
            int matrixSize = matrix.Length;
            int i, j, iOffset, jOffset, offCnt;
            int subMatrixSize;
            List<List<List<int>>> listOfMatrices = new List<List<List<int>>>();
            for (subMatrixSize = matrixSize; subMatrixSize > 1; subMatrixSize--)
            {
                offCnt = matrixSize - subMatrixSize + 1;
                for (iOffset = 0; iOffset < offCnt; iOffset++)
                {
                    for (jOffset = 0; jOffset < offCnt; jOffset++)
                    {
                        List<List<int>> curMatrix = new List<List<int>>();
                        for (i = 0; i < subMatrixSize; i++)
                        {
                            List<int> line = new List<int>();
                            for (j = 0; j < subMatrixSize; j++)
                            {
                                line.Add(matrix[i + iOffset][j + jOffset]);
                            }

                            curMatrix.Add(line);
                        }

                        listOfMatrices.Add(curMatrix);
                    }
                }
            }

            return listOfMatrices;
        }

        public static List<int> GetAllNeighbours(int[][] matrix, int i, int j)
        {
            List<int> allNeighbours = new List<int>();

            if (i > 0)
            {
                allNeighbours.Add(matrix[i - 1][j]);
            }

            if (j < matrix.Length - 1 && i > 0)
            {
                allNeighbours.Add(matrix[i - 1][j + 1]);
            }

            if (j < matrix.Length - 1)
            {
                allNeighbours.Add(matrix[i][j + 1]);
            }

            if (j < matrix.Length - 1 && i < matrix.Length - 1)
            {
                allNeighbours.Add(matrix[i + 1][j + 1]);
            }


            if (i < matrix.Length - 1)
            {
                allNeighbours.Add(matrix[i + 1][j]);
            }

            if (j > 0 && i < matrix.Length - 1)
            {
                allNeighbours.Add(matrix[i + 1][j - 1]);
            }

            if (j > 0)
            {
                allNeighbours.Add(matrix[i][j - 1]);
            }

            if (j > 0 && i > 0)
            {
                allNeighbours.Add(matrix[i - 1][j - 1]);
            }

            return allNeighbours;
        }

        public static List<int> GetBorder(List<List<int>> matrix)
        {
            List<int> border = new List<int>();

            for (int i = 0; i < matrix.Count; i++)
            {
                border.Add(matrix[0][i]);
            }

            for (int i = 1; i < matrix.Count - 1; i++)
            {
                border.Add(matrix[i][matrix.Count - 1]);
            }

            for (int i = matrix.Count - 1; i >= 0; i--)
            {
                border.Add(matrix[^1][i]);
            }

            for (int i = matrix.Count - 2; i >= 1; i--)
            {
                border.Add(matrix[i][0]);
            }

            return border;
        }

        public static bool DoesMatrixContainsSquareFibonacii(List<List<List<int>>> listOfMatrices)
        {
            for (int t = 0; t < listOfMatrices.Count; t++)
            {
                bool isFound = CheckIsBorderFibonacci(listOfMatrices[t]);
                if (isFound)
                {
                    return true;
                }
            }

            return false;
        }

        public static void Main(string[] args)
        {
            // Input
            int[][] matrix = new int[4][];

            matrix[0] = new int[] { 21, 1, 1, 1 };
            matrix[1] = new int[] { 14, 20, 2, 1 };
            matrix[2] = new int[] { 8, 5,  1,  1 };
            matrix[3] = new int[] { 21, 4, 5, 2 };

            var listOfMatrices = GenerateAllSquares(matrix);

            bool answer = DoesMatrixContainsSquareFibonacii(listOfMatrices);
            //Console.WriteLine(answer);

            int[] ordered = OrderByBits(new int[] { 8, 3, 15 });
            //Console.WriteLine(string.Join(" ", ordered));

            Console.WriteLine(String.Join(" ", GetAllNeighbours(matrix, 3, 2)));
            
            Console.WriteLine(CheckIsNeighboursFibonacci(matrix));
        }
    }
}
